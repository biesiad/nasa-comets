# Contribution

Your contribution is warmly welcome!

If you see something incorrect, please use an issue or contact with me directly.
If you want add or improve something - just make a fork & PR, fell free.

## TODO

✅ Good to see - languages versions.

✅ Add more formats (ie. xml).

✅ Add discoveries for >=2016

## Translations

For translations please make other branch for your changes & before PR (ie. for Spain will be: es_ES etc).

If you want, you can also add 'Translated by:' (ie. @JohnSmith) with link to your GitHub account or Twitter handle. Thank you!
