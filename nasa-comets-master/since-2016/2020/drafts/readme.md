Drafts for smaller, partial [updates](https://sungrazer.nrl.navy.mil/index.php?p=recent) - you can use use it before main update for each year/month. If you want add, please name like in an example 'reports-2020-4-15.csv' (date format: y-m-d; date of partial update post on the site).

Initial confirmations - confirmed by other participants (with little different structure - see an example).
